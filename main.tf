# Using a single workspace:
terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "gitLabOrg"

    workspaces {
      name = "test"
    }
  }
}

provider "azurerm" {
  features {}
  version         = "~>2.40.0"
  
}


resource "azurerm_resource_group" "example" {
  name     = "example"
  location = "West Europe"
}
